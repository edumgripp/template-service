FROM openjdk:8u111-jdk-alpine
ARG PROFILE=default
ENV PROFILE=${PROFILE}
VOLUME /tmp
ADD /target/*.jar app.jar
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTIONS  -Djava.security.egd=file:/dev/./urandom -Dspring.profiles.active=$PROFILE -jar /app.jar"]