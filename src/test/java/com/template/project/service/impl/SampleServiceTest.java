package com.template.project.service.impl;

import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;

import com.template.project.domain.dto.SampleDTO;
import com.template.project.persistence.dao.ISampleDAO;
import com.template.project.persistence.entity.Sample;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SampleServiceTest {
	
	@Mock
	ISampleDAO repository;
	
	@Mock
	ModelMapper modelMapper;
	
	@InjectMocks
	SampleService sampleService;

	@Test
	public void testGetSamples() {
	    long id1 = 1L;
	    String descricao1 = "Aqui vai a descricao";

		SampleDTO expectedSampleDTO = SampleDTO.builder()
				.id(id1)
				.descricao(descricao1)
				.build();
		
		Sample expectedSample = Sample.builder()
				.id(id1)
				.descricao(descricao1)
				.build();
		
	    long id2 = 2L;
	    String descricao2 = "Aqui vai outra descricao";

		SampleDTO expectedSampleDTO2 = SampleDTO.builder()
				.id(id2)
				.descricao(descricao2)
				.build();
		
		Sample expectedSample2 = Sample.builder()
				.id(id2)
				.descricao(descricao2)
				.build();
		
		when(repository.findAll()).thenReturn(Arrays.asList(expectedSample, expectedSample2));
		when(modelMapper.map(expectedSample, SampleDTO.class)).thenReturn(expectedSampleDTO);
		when(modelMapper.map(expectedSample2, SampleDTO.class)).thenReturn(expectedSampleDTO2);
		
		List<SampleDTO> createdSamplesDTO = sampleService.getSamples();
		
		List<SampleDTO> expectedSamplesDTO = Arrays.asList(expectedSampleDTO, expectedSampleDTO2);
		for (int i = 0; i < createdSamplesDTO.size(); i++ ) {
			assertThat(createdSamplesDTO.get(i), samePropertyValuesAs(expectedSamplesDTO.get(i)));
		}
	}

	@Test
	public void testGetSample() {
	    long id = 1L;
	    String descricao = "Aqui vai a descricao";

		SampleDTO expectedSampleDTO = SampleDTO.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		Sample expectedSample = Sample.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		
		Optional<Sample> existSample = Optional.ofNullable(expectedSample);
	
		doReturn(existSample).when(repository).findById(id);
		when(modelMapper.map(expectedSample, SampleDTO.class)).thenReturn(expectedSampleDTO);
		
		SampleDTO createdSampleDTO = sampleService.getSample(id);
		
        assertThat(createdSampleDTO, samePropertyValuesAs(expectedSampleDTO));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetSampleWhenSampleInvalido() {
	    long id = 1L;
		
		Optional<Sample> doesNotExistSample = Optional.empty();
		
		doReturn(doesNotExistSample).when(repository).findById(id);
		
		sampleService.getSample(id);
	}
	
	@Test
	public void testCreateSample() {
	    long id = 1L;
	    String descricao = "Aqui vai a descricao";

		SampleDTO expectedSampleDTO = SampleDTO.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		Sample expectedSample = Sample.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		
		Example<Sample> example = Example.of(expectedSample);
		Optional<Sample> doesNotExistSample = Optional.empty();
	
		doReturn(doesNotExistSample).when(repository).findOne(example);
		doReturn(expectedSample).when(repository).save(expectedSample);
		when(modelMapper.map(expectedSample, SampleDTO.class)).thenReturn(expectedSampleDTO);
		when(modelMapper.map(expectedSampleDTO, Sample.class)).thenReturn(expectedSample);
		
		SampleDTO createdSampleDTO = sampleService.createSample(expectedSampleDTO);
		
        assertThat(createdSampleDTO, samePropertyValuesAs(expectedSampleDTO));
	}
	
	@Test
	public void testCreateSampleWhenSampleExists() {
	    long id = 1L;
	    String descricao = "Aqui vai a descricao";

		SampleDTO expectedSampleDTO = SampleDTO.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		Sample expectedSample = Sample.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		
		Example<Sample> example = Example.of(expectedSample);
		Optional<Sample> existSample = Optional.ofNullable(expectedSample);
	
		doReturn(existSample).when(repository).findOne(example);
		when(modelMapper.map(expectedSample, SampleDTO.class)).thenReturn(expectedSampleDTO);
		when(modelMapper.map(expectedSampleDTO, Sample.class)).thenReturn(expectedSample);
		
		SampleDTO createdSampleDTO = sampleService.createSample(expectedSampleDTO);
		
        assertThat(createdSampleDTO, samePropertyValuesAs(expectedSampleDTO));
	}
	
	@Test
	public void testUpdateSample() {

	    long id = 1L;
	    String descricao = "Aqui vai a descricao";

		SampleDTO expectedSampleDTO = SampleDTO.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		Sample expectedSample = Sample.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		
		doReturn(expectedSample).when(repository).save(expectedSample);
		when(modelMapper.map(expectedSampleDTO, Sample.class)).thenReturn(expectedSample);
		
		SampleDTO createdSampleDTO = sampleService.updateSample(expectedSampleDTO,id);
		
        assertThat(createdSampleDTO, samePropertyValuesAs(expectedSampleDTO));
	}
	
	@Test
	public void testDeleteSample() {
		long id = 1L;
		
		doNothing().when(repository).deleteById(id);
		
		sampleService.deleteSample(id);
		
		verify(repository).deleteById(id);
	}
}
