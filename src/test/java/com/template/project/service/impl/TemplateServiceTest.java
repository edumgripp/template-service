package com.template.project.service.impl;

import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;

import com.template.project.domain.dto.TemplateDTO;
import com.template.project.persistence.dao.ITemplateDAO;
import com.template.project.persistence.entity.Template;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TemplateServiceTest {
	
	@Mock
	ITemplateDAO repository;
	
	@Mock
	ModelMapper modelMapper;
	
	@InjectMocks
	TemplateService templateService;

	@Test
	public void testGetTemplates() {
	    long id1 = 1L;
	    String descricao1 = "Aqui vai a descricao";

		TemplateDTO expectedTemplateDTO = TemplateDTO.builder()
				.id(id1)
				.descricao(descricao1)
				.build();
		
		Template expectedTemplate = Template.builder()
				.id(id1)
				.descricao(descricao1)
				.build();
		
	    long id2 = 2L;
	    String descricao2 = "Aqui vai outra descricao";

		TemplateDTO expectedTemplateDTO2 = TemplateDTO.builder()
				.id(id2)
				.descricao(descricao2)
				.build();
		
		Template expectedTemplate2 = Template.builder()
				.id(id2)
				.descricao(descricao2)
				.build();
		
		when(repository.findAll()).thenReturn(Arrays.asList(expectedTemplate, expectedTemplate2));
		when(modelMapper.map(expectedTemplate, TemplateDTO.class)).thenReturn(expectedTemplateDTO);
		when(modelMapper.map(expectedTemplate2, TemplateDTO.class)).thenReturn(expectedTemplateDTO2);
		
		List<TemplateDTO> createdTemplatesDTO = templateService.getTemplates();
		
		List<TemplateDTO> expectedTemplatesDTO = Arrays.asList(expectedTemplateDTO, expectedTemplateDTO2);
		for (int i = 0; i < createdTemplatesDTO.size(); i++ ) {
			assertThat(createdTemplatesDTO.get(i), samePropertyValuesAs(expectedTemplatesDTO.get(i)));
		}
	}

	@Test
	public void testGetTemplate() {
	    long id = 1L;
	    String descricao = "Aqui vai a descricao";

		TemplateDTO expectedTemplateDTO = TemplateDTO.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		Template expectedTemplate = Template.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		
		Optional<Template> existTemplate = Optional.ofNullable(expectedTemplate);
	
		doReturn(existTemplate).when(repository).findById(id);
		when(modelMapper.map(expectedTemplate, TemplateDTO.class)).thenReturn(expectedTemplateDTO);
		
		TemplateDTO createdTemplateDTO = templateService.getTemplate(id);
		
        assertThat(createdTemplateDTO, samePropertyValuesAs(expectedTemplateDTO));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetTemplateWhenTemplateInvalido() {
	    long id = 1L;
		
		Optional<Template> doesNotExistTemplate = Optional.empty();
		
		doReturn(doesNotExistTemplate).when(repository).findById(id);
		
		templateService.getTemplate(id);
	}
	
	@Test
	public void testCreateTemplate() {
	    long id = 1L;
	    String descricao = "Aqui vai a descricao";

		TemplateDTO expectedTemplateDTO = TemplateDTO.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		Template expectedTemplate = Template.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		
		Example<Template> example = Example.of(expectedTemplate);
		Optional<Template> doesNotExistTemplate = Optional.empty();
	
		doReturn(doesNotExistTemplate).when(repository).findOne(example);
		doReturn(expectedTemplate).when(repository).save(expectedTemplate);
		when(modelMapper.map(expectedTemplate, TemplateDTO.class)).thenReturn(expectedTemplateDTO);
		when(modelMapper.map(expectedTemplateDTO, Template.class)).thenReturn(expectedTemplate);
		
		TemplateDTO createdTemplateDTO = templateService.createTemplate(expectedTemplateDTO);
		
        assertThat(createdTemplateDTO, samePropertyValuesAs(expectedTemplateDTO));
	}
	
	@Test
	public void testCreateTemplateWhenTemplateExists() {
	    long id = 1L;
	    String descricao = "Aqui vai a descricao";

		TemplateDTO expectedTemplateDTO = TemplateDTO.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		Template expectedTemplate = Template.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		
		Example<Template> example = Example.of(expectedTemplate);
		Optional<Template> existTemplate = Optional.ofNullable(expectedTemplate);
	
		doReturn(existTemplate).when(repository).findOne(example);
		when(modelMapper.map(expectedTemplate, TemplateDTO.class)).thenReturn(expectedTemplateDTO);
		when(modelMapper.map(expectedTemplateDTO, Template.class)).thenReturn(expectedTemplate);
		
		TemplateDTO createdTemplateDTO = templateService.createTemplate(expectedTemplateDTO);
		
        assertThat(createdTemplateDTO, samePropertyValuesAs(expectedTemplateDTO));
	}
	
	@Test
	public void testUpdateTemplate() {

	    long id = 1L;
	    String descricao = "Aqui vai a descricao";

		TemplateDTO expectedTemplateDTO = TemplateDTO.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		Template expectedTemplate = Template.builder()
				.id(id)
				.descricao(descricao)
				.build();
		
		
		doReturn(expectedTemplate).when(repository).save(expectedTemplate);
		when(modelMapper.map(expectedTemplateDTO, Template.class)).thenReturn(expectedTemplate);
		
		TemplateDTO createdTemplateDTO = templateService.updateTemplate(expectedTemplateDTO,id);
		
        assertThat(createdTemplateDTO, samePropertyValuesAs(expectedTemplateDTO));
	}
	
	@Test
	public void testDeleteTemplate() {
		long id = 1L;
		
		doNothing().when(repository).deleteById(id);
		
		templateService.deleteTemplate(id);
		
		verify(repository).deleteById(id);
	}
}
