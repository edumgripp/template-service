package com.template.project.persistence.dao;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.template.project.persistence.dao.ITemplateDAO;
import com.template.project.persistence.entity.Template;

@DataJpaTest
@TestMethodOrder(OrderAnnotation.class)
class TemplateDAOTest {

	@Autowired
	ITemplateDAO repository;

	@Test
	@Rollback(false)
	@Order(1)
	void testCreateTemplate() {

		Template template = new Template(1l, "descricao teste");

		template = repository.save(template);

		assertEquals(1l, template.getId());

	}

	@Test
	@Order(2)
	void testFindTemplateById() {

		Optional<Template> template = null;

		template = repository.findById(1l);

		assertTrue(template.isPresent());

	}

	@Test
	@Order(3)
	void testListTemplatess() {
		List<Template> templates = null;

		templates = repository.findAll();
		assertEquals(1, templates.size());

	}

	@Test
	@Rollback(false)
	@Order(4)
	void testUpdateTemplate() {
		Template template = new Template(1l, "descricao atualizada");

		template = repository.save(template);

		assertEquals(1l, template.getId());
		assertEquals("descricao atualizada", template.getDescricao());

	}

	@Test
	@Rollback(false)
	@Order(5)
	void testDeleteTemplate() {
		repository.deleteById(1l);

		List<Template> templates = repository.findAll();
		assertTrue(templates.isEmpty());
	}

}
