package com.template.project.persistence.dao;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.template.project.persistence.dao.ISampleDAO;
import com.template.project.persistence.entity.Sample;

@DataJpaTest
@TestMethodOrder(OrderAnnotation.class)
class SampleDAOTest {

	@Autowired
	ISampleDAO repository;

	@Test
	@Rollback(false)
	@Order(1)
	void testCreateSample() {

		Sample sample = new Sample(1l, "descricao teste");

		sample = repository.save(sample);

		assertEquals(1l, sample.getId());

	}

	@Test
	@Order(2)
	void testFindSampleById() {

		Optional<Sample> sample = null;

		sample = repository.findById(1l);

		assertTrue(sample.isPresent());

	}

	@Test
	@Order(3)
	void testListSampless() {
		List<Sample> samples = null;

		samples = repository.findAll();
		assertEquals(1, samples.size());

	}

	@Test
	@Rollback(false)
	@Order(4)
	void testUpdateSample() {
		Sample sample = new Sample(1l, "descricao atualizada");

		sample = repository.save(sample);

		assertEquals(1l, sample.getId());
		assertEquals("descricao atualizada", sample.getDescricao());

	}

	@Test
	@Rollback(false)
	@Order(5)
	void testDeleteSample() {
		repository.deleteById(1l);

		List<Sample> samples = repository.findAll();
		assertTrue(samples.isEmpty());
	}

}
