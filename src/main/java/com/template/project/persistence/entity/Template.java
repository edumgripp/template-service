package com.template.project.persistence.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TEMPLATE")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Template {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	private long id;
	
	private String descricao;

}
