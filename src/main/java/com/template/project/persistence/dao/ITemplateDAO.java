package com.template.project.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.template.project.persistence.entity.Template;

public interface ITemplateDAO extends JpaRepository<Template, Long> {
	
	
}
