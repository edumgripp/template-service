package com.template.project.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.template.project.persistence.entity.Sample;

public interface ISampleDAO extends JpaRepository<Sample, Long> {
	
	
}
