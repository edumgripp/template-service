package com.template.project.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.template.project.domain.dto.SampleDTO;
import com.template.project.persistence.dao.ISampleDAO;
import com.template.project.persistence.entity.Sample;
import com.template.project.service.interfaces.ISampleService;

@Service
public class SampleService implements ISampleService{
	
	@Autowired
	ISampleDAO repository;
	
	@Autowired
	ModelMapper modelMapper;

	@Override
	public List<SampleDTO> getSamples() {

		List<SampleDTO> returnSamples = new ArrayList<>();
		repository.findAll().forEach(sample -> returnSamples.add(convertToDto(sample)));
		
		return returnSamples;
		
	}

	@Override
	public SampleDTO getSample(long id) {
		Optional<Sample> sample = repository.findById(id);

		if (!sample.isPresent()) {
			throw new IllegalArgumentException("Sample invalido id-" + id);
		}

		return convertToDto(sample.get());
	}

	@Override
	public SampleDTO createSample(SampleDTO sampleDTO) {
		Example<Sample> exampleAcessoCLiente = Example.of(convertToEntity(sampleDTO));
		Optional<Sample> sampleOld = repository.findOne(exampleAcessoCLiente);

		if (sampleOld.isPresent()) {
			return convertToDto(sampleOld.get());
		} else {
			sampleDTO = convertToDto(repository.save(convertToEntity(sampleDTO)));
		}

		return sampleDTO;
	}

	@Override
	public SampleDTO updateSample(SampleDTO sampleDTO, long id) {
		
		sampleDTO.setId(id);
		repository.save(convertToEntity(sampleDTO));
		return sampleDTO;
	}

	@Override
	public void deleteSample(Long id) {
		repository.deleteById(id);
		
		
	}

	private SampleDTO convertToDto(Sample sample) {

		return modelMapper.map(sample, SampleDTO.class);
	}
	
	private Sample convertToEntity(SampleDTO sampleDTO) {

		return modelMapper.map(sampleDTO, Sample.class);
	}
}
