package com.template.project.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.template.project.domain.dto.TemplateDTO;
import com.template.project.persistence.dao.ITemplateDAO;
import com.template.project.persistence.entity.Template;
import com.template.project.service.interfaces.ITemplateService;

@Service
public class TemplateService implements ITemplateService{
	
	@Autowired
	ITemplateDAO repository;
	
	@Autowired
	ModelMapper modelMapper;

	@Override
	public List<TemplateDTO> getTemplates() {

		List<TemplateDTO> returnTemplates = new ArrayList<>();
		repository.findAll().forEach(template -> returnTemplates.add(convertToDto(template)));
		
		return returnTemplates;
		
	}

	@Override
	public TemplateDTO getTemplate(long id) {
		Optional<Template> template = repository.findById(id);

		if (!template.isPresent()) {
			throw new IllegalArgumentException("Template invalido id-" + id);
		}

		return convertToDto(template.get());
	}

	@Override
	public TemplateDTO createTemplate(TemplateDTO templateDTO) {
		Example<Template> exampleAcessoCLiente = Example.of(convertToEntity(templateDTO));
		Optional<Template> templateOld = repository.findOne(exampleAcessoCLiente);

		if (templateOld.isPresent()) {
			return convertToDto(templateOld.get());
		} else {
			templateDTO = convertToDto(repository.save(convertToEntity(templateDTO)));
		}

		return templateDTO;
	}

	@Override
	public TemplateDTO updateTemplate(TemplateDTO templateDTO, long id) {
		
		templateDTO.setId(id);
		repository.save(convertToEntity(templateDTO));
		return templateDTO;
	}

	@Override
	public void deleteTemplate(Long id) {
		repository.deleteById(id);
		
		
	}

	private TemplateDTO convertToDto(Template template) {

		return modelMapper.map(template, TemplateDTO.class);
	}
	
	private Template convertToEntity(TemplateDTO templateDTO) {

		return modelMapper.map(templateDTO, Template.class);
	}
}
