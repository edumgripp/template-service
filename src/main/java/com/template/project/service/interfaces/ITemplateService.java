package com.template.project.service.interfaces;

import java.util.List;

import com.template.project.domain.dto.TemplateDTO;

public interface ITemplateService {

	List<TemplateDTO> getTemplates();

	TemplateDTO getTemplate(long id);

	TemplateDTO createTemplate(TemplateDTO templateDTO);

	TemplateDTO updateTemplate(TemplateDTO templateDTO, long id);

	void deleteTemplate(Long id);

}
