package com.template.project.service.interfaces;

import java.util.List;

import com.template.project.domain.dto.SampleDTO;

public interface ISampleService {

	List<SampleDTO> getSamples();

	SampleDTO getSample(long id);

	SampleDTO createSample(SampleDTO sampleDTO);

	SampleDTO updateSample(SampleDTO sampleDTO, long id);

	void deleteSample(Long id);

}
