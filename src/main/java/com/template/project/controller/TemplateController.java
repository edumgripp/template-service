package com.template.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.template.project.domain.dto.TemplateDTO;
import com.template.project.service.interfaces.ITemplateService;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("/backoffice/template")
@Api(tags = "Template")
@RestController
@Slf4j
@Validated
public class TemplateController {

	@Autowired
	ITemplateService templateService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public List<TemplateDTO> getTemplates() {
		
		return templateService.getTemplates();
	}

	@GetMapping(value ="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public TemplateDTO retrieveTemplate(@PathVariable long id) {

		return templateService.getTemplate(id);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public TemplateDTO createTemplate(@RequestBody TemplateDTO templateDTO) {
		log.debug("criando template");
		return templateService.createTemplate(templateDTO);

	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public TemplateDTO updateTemplate(@RequestBody TemplateDTO templateDTO, @PathVariable long id) {

		return templateService.updateTemplate(templateDTO, id);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteTemplate(@PathVariable Long id) {

		templateService.deleteTemplate(id);
	}

}
