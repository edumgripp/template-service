package com.template.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.template.project.domain.dto.SampleDTO;
import com.template.project.service.interfaces.ISampleService;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("/backoffice/sample")
@Api(tags = "Sample")
@RestController
@Slf4j
@Validated
public class SampleController {

	@Autowired
	ISampleService sampleService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public List<SampleDTO> getSamples() {
		
		return sampleService.getSamples();
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public SampleDTO retrieveSample(@PathVariable long id) {

		return sampleService.getSample(id);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public SampleDTO createSample(@RequestBody SampleDTO sampleDTO) {
		log.debug("criando sample");
		return sampleService.createSample(sampleDTO);

	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public SampleDTO updateSample(@RequestBody SampleDTO sampleDTO, @PathVariable long id) {

		return sampleService.updateSample(sampleDTO, id);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteSample(@PathVariable Long id) {

		sampleService.deleteSample(id);
	}

}
